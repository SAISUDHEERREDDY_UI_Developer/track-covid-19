﻿using Covid19.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Covid19.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CountryReport : ContentPage
    {
        public CountryReport()
        {
            InitializeComponent();
            BindingContext = App.AppSetup.GloalViewModel; //new GloalViewModel();
           // BindingContext = App.AppSetup.CountryReportViewModel;
        }

        void OnEmptyViewSwitchToggled(object sender, ToggledEventArgs e)
        {
            ToggleEmptyView((sender as Switch).IsToggled);
        }

        void ToggleEmptyView(bool isToggled)
        {
            collectionView.EmptyView = isToggled ? Resources["BasicEmptyView"] : Resources["AdvancedEmptyView"];
        }

       

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(e.NewTextValue))
            {
                //list.ItemsSource = tempdata;
                collectionView.ItemsSource = App.AppSetup.GloalViewModel.AllCountry;
            }

            else
            {
                // App.Locator.StoreViewModel.WishFilterCommand.Execute(e.NewTextValue);
                collectionView.ItemsSource = App.AppSetup.GloalViewModel.AllCountry.Where(x => x.country.ToLower().Contains(e.NewTextValue.ToLower())).ToList();

                //list.ItemsSource = tempdata.Where(x => x.Name.StartsWith(e.NewTextValue));
            }
        }
    }
}