﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Covid19.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GlobalReport : ContentPage
    {
        public GlobalReport()
        {
            InitializeComponent();
            BindingContext = App.AppSetup.GloalViewModel;
           // Device.BeginInvokeOnMainThread(() =>
            //App.AppSetup.GloalViewModel.GetGlobalDataCommand.Execute(null));
        }

    }
}