﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19.Models
{
    public class BaseResponse
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool success { get; set; }
    }
}
