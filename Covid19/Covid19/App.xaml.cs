﻿using CommonServiceLocator;
using Covid19.Views;
using GalaSoft.MvvmLight.Ioc;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Covid19
{
    public partial class App : Application
    {
        #region Properties

        public static string BaseImageUrl { get; } = "https://cdn.syncfusion.com/essential-ui-kit-for-xamarin.forms/common/uikitimages/";

        #endregion
        private static AppSetup appSetup { get; set; }
        public static AppSetup AppSetup => appSetup;
        public App()
        {
            InitializeComponent();
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<AppSetup>();
            appSetup = ServiceLocator.Current.GetInstance<AppSetup>();
            Device.SetFlags(new string[] {  "RadioButton_Experimental",
                                            "AppTheme_Experimental",
                                            "Markup_Experimental",
                                            "Expander_Experimental" ,
                                            "CarouselView_Experimental", 
                                            "SwipeView_Experimental", 
                                            "IndicatorView_Experimental"});

              MainPage =/* new MainPage();// */new Views.CovidTabbedPage();// MainPage();
            //MainPage = new LandingPage();


        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
