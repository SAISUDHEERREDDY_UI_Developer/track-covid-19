﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Covid19.Managers.Providers
{
    public class ApiProvider : IApiProvider
    {
        #region NonAsync
        //public ApiProvider()
        //{
        //    HttpClientHandler handler = new HttpClientHandler();

        //    _httpClient = new HttpClient(handler);
        //    TimeSpan ts = TimeSpan.FromMilliseconds(100000);
        //    _httpClient.Timeout = ts;
        //}
        //private readonly HttpClient _httpClient;
        //public ApiResult<T> Get<T>(string url, Dictionary<string, string> headers = null)
        //{
        //    HttpResponseMessage result = null;
        //    try
        //    {
        //        lock (_httpClient)
        //        {
        //            if (headers != null)
        //            {
        //                AddHeadersToClient(headers);
        //            }
        //            result = _httpClient.GetAsync(url).Result;
        //            if (headers != null)
        //            {
        //                RemoveHeadersFromClient(headers);
        //            }
        //        }

        //        var rawResult = result.Content.ReadAsStringAsync().Result;

        //        try
        //        {
        //            var deserialized = JsonConvert.DeserializeObject<T>(rawResult);
        //            return new ApiResult<T>(rawResult, (int)result.StatusCode, deserialized);
        //        }
        //        catch (Exception ex)
        //        {
        //            return new ApiResult<T>(rawResult, 501, Activator.CreateInstance<T>());
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Debug.WriteLine("Error Message is :-" + e.Message);
        //    }

        //    return new ApiResult<T>(null, null != result ? (int)result.StatusCode : 0, default(T));
        //}

        //public ApiResult<T> Post<T, TR>(string url, TR body, Dictionary<string, string> headers = null)
        //{
        //    HttpResponseMessage result = null;
        //    try
        //    {
        //        lock (_httpClient)
        //        {
        //            if (headers != null)
        //            {
        //                AddHeadersToClient(headers);
        //            }
        //            //var x = JsonConvert.SerializeObject(body);
        //            result = _httpClient.PostAsync(url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json")).Result;
        //            if (headers != null)
        //            {
        //                RemoveHeadersFromClient(headers);
        //            }
        //        }

        //        var rawResult = result.Content.ReadAsStringAsync().Result;
        //        try
        //        {
        //            var deserialized = JsonConvert.DeserializeObject<T>(rawResult);
        //            return new ApiResult<T>(rawResult, (int)result.StatusCode, deserialized);
        //        }
        //        catch (Exception ex)
        //        {
        //            return new ApiResult<T>(rawResult, 501, Activator.CreateInstance<T>());
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Debug.WriteLine("Error Message is :-" + e.Message);
        //    }

        //    return new ApiResult<T>(null, null != result ? (int)result.StatusCode : 0, default(T));
        //}

        //public ApiResult<T> Delete<T>(string url, Dictionary<string, string> headers = null)
        //{
        //    HttpResponseMessage result = null;
        //    try
        //    {
        //        lock (_httpClient)
        //        {
        //            if (headers != null)
        //            {
        //                AddHeadersToClient(headers);
        //            }
        //            result = _httpClient.DeleteAsync(url).Result;
        //            if (headers != null)
        //            {
        //                RemoveHeadersFromClient(headers);
        //            }
        //        }

        //        var rawResult = result.Content.ReadAsStringAsync().Result;
        //        try
        //        {
        //            var deserialized = JsonConvert.DeserializeObject<T>(rawResult);
        //            return new ApiResult<T>(rawResult, (int)result.StatusCode, deserialized);
        //        }
        //        catch (Exception ex)
        //        {
        //            return new ApiResult<T>(rawResult, 501, Activator.CreateInstance<T>());
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Debug.WriteLine("Error Message is :-" + e.Message);
        //    }

        //    return new ApiResult<T>(null, null != result ? (int)result.StatusCode : 0, default(T));
        //}

        //public ApiResult<T> Put<T, TR>(string url, TR body, Dictionary<string, string> headers = null)
        //{
        //    HttpResponseMessage result = null;
        //    try
        //    {
        //        lock (_httpClient)
        //        {
        //            if (headers != null)
        //            {
        //                AddHeadersToClient(headers);
        //            }
        //            result = _httpClient.PutAsync(url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json")).Result;
        //            if (headers != null)
        //            {
        //                RemoveHeadersFromClient(headers);
        //            }
        //        }

        //        var rawResult = result.Content.ReadAsStringAsync().Result;

        //        try
        //        {
        //            var deserialized = JsonConvert.DeserializeObject<T>(rawResult);
        //            return new ApiResult<T>(rawResult, (int)result.StatusCode, deserialized);
        //        }
        //        catch (Exception ex)
        //        {
        //            return new ApiResult<T>(rawResult, 501, Activator.CreateInstance<T>());
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Debug.WriteLine("Error Message is :-" + e.Message);
        //    }

        //    return new ApiResult<T>(null, null != result ? (int)result.StatusCode : 0, default(T));
        //}

        //void AddHeadersToClient(Dictionary<string, string> headers)
        //{
        //    foreach (var kv in headers)
        //    {

        //        _httpClient.DefaultRequestHeaders.Add(kv.Key, kv.Value);
        //    }
        //}

        //void RemoveHeadersFromClient(Dictionary<string, string> headers)
        //{
        //    foreach (var kv in headers)
        //    {
        //        _httpClient.DefaultRequestHeaders.Remove(kv.Key);
        //    }
        //}

        //public void SaveCookies(string path)
        //{
        //    throw new NotImplementedException();
        //}

        //public void LoadCookies(string path)
        //{
        //    throw new NotImplementedException();

        //}

        //public void DeleteCookies(string path)
        //{
        //    throw new NotImplementedException();
        //}

        //public void ExpireCookies()
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

        #region Async

        private readonly HttpClient _httpClient;
        public ApiProvider()
        {
            //_cookieContainer = new CookieContainer();
            //HttpClientHandler handler = new HttpClientHandler
            //{
            //    UseCookies = true,
            //    UseDefaultCredentials = false
            //};

            HttpClientHandler handler = new HttpClientHandler();
            _httpClient = new HttpClient(handler);
            TimeSpan ts = TimeSpan.FromMilliseconds(100000);
            _httpClient.Timeout = ts;
        }
        public async Task<ApiResult<T>> Get<T>(string url, Dictionary<string, string> headers = null)
        {
            HttpResponseMessage result = null;
            try
            {
                lock (_httpClient)
                {
                    if (headers != null)
                    {
                        AddHeadersToClient(headers);
                    }
                    result = _httpClient.GetAsync(url).Result;
                    if (headers != null)
                    {
                        RemoveHeadersFromClient(headers);
                    }
                }
                var rawResult = result.Content.ReadAsStringAsync().Result;
                try
                {
                    var deserialized = JsonConvert.DeserializeObject<T>(rawResult);
                    return new ApiResult<T>(rawResult, (int)result.StatusCode, deserialized);
                }
                catch (Exception e)
                {
                    return new ApiResult<T>(rawResult, 501, Activator.CreateInstance<T>());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error Message is :-" + ex.Message);
            }
            return new ApiResult<T>(null, null != result ? (int)result.StatusCode : 0, default(T));
        }

        public async Task<ApiResult<T>> Post<T, TR>(string url, TR body, Dictionary<string, string> headers = null)
        {
            HttpResponseMessage result = null;
            try
            {
                lock (_httpClient)
                {
                    if (headers != null)
                    {
                        AddHeadersToClient(headers);
                    }
                    var json = JsonConvert.SerializeObject(body);
                    var str = new StringContent(JsonConvert.SerializeObject(body));
                    result = _httpClient.PostAsync(url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json")).Result;

                    if (headers != null)
                    {
                        RemoveHeadersFromClient(headers);
                    }
                }

                var rawResult = result.Content.ReadAsStringAsync().Result;
                try
                {
                    var deserialized = JsonConvert.DeserializeObject<T>(rawResult);
                    return new ApiResult<T>(rawResult, (int)result.StatusCode, deserialized);
                }
                catch (Exception e)
                {
                    return new ApiResult<T>(rawResult, 501, Activator.CreateInstance<T>());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error Message is :-" + ex.Message);
            }

            return new ApiResult<T>(null, null != result ? (int)result.StatusCode : 0, default(T));
        }

        public async Task<ApiResult<T>> Delete<T>(string url, Dictionary<string, string> headers = null)
        {
            HttpResponseMessage result = null;
            try
            {
                lock (_httpClient)
                {
                    if (headers != null)
                    {
                        AddHeadersToClient(headers);
                    }
                    result = _httpClient.DeleteAsync(url).Result;
                    if (headers != null)
                    {
                        RemoveHeadersFromClient(headers);
                    }
                }

                var rawResult = result.Content.ReadAsStringAsync().Result;
                try
                {
                    var deserialized = JsonConvert.DeserializeObject<T>(rawResult);
                    return new ApiResult<T>(rawResult, (int)result.StatusCode, deserialized);
                }
                catch (Exception e)
                {
                    return new ApiResult<T>(rawResult, 501, Activator.CreateInstance<T>());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error Message is :-" + ex.Message);
            }

            return new ApiResult<T>(null, null != result ? (int)result.StatusCode : 0, default(T));
        }

        public async Task<ApiResult<T>> Put<T, TR>(string url, TR body, Dictionary<string, string> headers = null)
        {
            HttpResponseMessage result = null;
            try
            {
                lock (_httpClient)
                {
                    if (headers != null)
                    {
                        AddHeadersToClient(headers);
                    }
                    result = _httpClient.PutAsync(url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json")).Result;
                    if (headers != null)
                    {
                        RemoveHeadersFromClient(headers);
                    }
                }

                var rawResult = result.Content.ReadAsStringAsync().Result;

                try
                {
                    var deserialized = JsonConvert.DeserializeObject<T>(rawResult);
                    return new ApiResult<T>(rawResult, (int)result.StatusCode, deserialized);
                }
                catch (Exception e)
                {
                    return new ApiResult<T>(rawResult, 501, Activator.CreateInstance<T>());
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error Message is :-" + ex.Message);
            }
            return new ApiResult<T>(null, null != result ? (int)result.StatusCode : 0, default(T));
        }

        public void SaveCookies(string path)
        {
            throw new NotImplementedException();
        }

        public void LoadCookies(string path)
        {
            throw new NotImplementedException();
        }

        public void DeleteCookies(string path)
        {
            throw new NotImplementedException();
        }

        void AddHeadersToClient(Dictionary<string, string> headers)
        {
            foreach (var kv in headers)
            {
                _httpClient.DefaultRequestHeaders.Add(kv.Key, kv.Value);
            }
        }

        void RemoveHeadersFromClient(Dictionary<string, string> headers)
        {
            foreach (var kv in headers)
            {
                _httpClient.DefaultRequestHeaders.Remove(kv.Key);
            }
        }

        #endregion
    }
}
