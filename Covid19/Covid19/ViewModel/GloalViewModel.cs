﻿using Acr.UserDialogs;
using Covid19.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Covid19.ViewModel
{
   public class GloalViewModel:BaseViewModel
    {
        public GloalViewModel() 
        {
            GetGlobalCoronaDataCommand.Execute(null);

            OSAppTheme currentTheme = Application.Current.RequestedTheme;
            //if (currentTheme == OSAppTheme.Dark)
            //    CarouselList = new List<string>() { "lightcar01.png", "lightcar02.png", "lightcar03.png" };
            //else
                CarouselList = new List<string>() { "darkcar01.png", "darkcar02.png", "darkcar03.png" };
        }
        #region Properties 

        private bool isbussy=false;

        public bool IsBussy 
        {
            get { return isbussy; }
            set { isbussy = value; RaisePropertyChanged(() =>IsBussy); }
        }



        private List<string> carouselList;
        public List<string> CarouselList
        {
            get { return carouselList; }
            set { carouselList = value; RaisePropertyChanged(() => CarouselList); }
        }

         
   

        private CountryModel globalcovid;
        public CountryModel GlobalCovid
        {
            get { return globalcovid; }
            set { globalcovid = value; RaisePropertyChanged(() => GlobalCovid); }
        } 

        private List<CountryModel> allcountry;

        public List<CountryModel> AllCountry
        {
            get { return allcountry; }
            set { allcountry = value; RaisePropertyChanged(() => AllCountry); }
        }
        
        private ObservableCollection<CountryModel> searchcountry;
        public ObservableCollection<CountryModel> SearchCountry
        {
            get { return searchcountry; }
            set { searchcountry = value; RaisePropertyChanged(() => SearchCountry); }
        }

        private List<CountryFlag> countryflags;

        public List<CountryFlag> CountryFlags
        {
            get { return countryflags; }
            set { countryflags = value;RaisePropertyChanged(()=>CountryFlags); }
        }

        #endregion

        #region Commands
        public Command GetGlobalCoronaDataCommand => new Command(async()=>await GetGlobalCoronaDataCommandExecution()); 
        #endregion

        #region CommandExecution
        private async Task GetGlobalCoronaDataCommandExecution()
        {
            var current = Xamarin.Essentials.Connectivity.NetworkAccess;
            if (current != Xamarin.Essentials.NetworkAccess.Internet)
                UserDialogs.Instance.Alert("Please Check the internet connection", "Network Error", "OK");
            else
            {
                IsBussy = true;
               // UserDialogs.Instance.ShowLoading();
                await Task.Delay(1000);
                using (var client = new HttpClient())
                {
                    try
                    {

                    client.BaseAddress = new Uri("https://coronavirus-19-api.herokuapp.com/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); 

                    //GET Method
                    HttpResponseMessage result = await client.GetAsync("countries");
                    if (result.IsSuccessStatusCode)
                    {
                        var rawResult = result.Content.ReadAsStringAsync().Result;
                        AllCountry = JsonConvert.DeserializeObject<List<CountryModel>>(rawResult);
                        SearchCountry = new ObservableCollection<CountryModel>(AllCountry);
                        GlobalCovid = AllCountry.Where(e => e.country == "World").SingleOrDefault();
                        App.AppSetup.HomeViewModel.Country = AllCountry.Where(e => e.country == "India").SingleOrDefault();
                        IsBussy = false;
                        //UserDialogs.Instance.HideLoading();
                        await GetCountryDataCommandExecution();
                    }
                    else
                    {
                        IsBussy = false;
                        UserDialogs.Instance.Alert("Internal server Error", "Server Error", "OK");
                        }
                    }
                    catch (Exception ex)
                    {

                        //UserDialogs.Instance.Alert("Internal Error", $"Internal Error {ex.Message}", "OK");
                    }
                }
            }
        }
        #endregion

        #region Methods
        private async Task GetCountryDataCommandExecution()
        {
            UserDialogs.Instance.ShowLoading();

            // Read RecipeData.json from this Assembly's DataModel folder
            var assembly = GetType().Assembly;
            var resourceName = assembly.GetManifestResourceNames().FirstOrDefault(x => x.EndsWith("Covid19.Resources.countryflag.json", System.StringComparison.OrdinalIgnoreCase));
            if (string.IsNullOrEmpty(resourceName))
            {
                Debugger.Break();
                UserDialogs.Instance.Alert("Not find the json file","Alert","OK");
                //   return new List<RecipeGroup>();
            }

            // Parse the JSON and generate a collection of RecipeGroup objects
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                string json = await reader.ReadToEndAsync();
                CountryFlags = JsonConvert.DeserializeObject<List<CountryFlag>>(json);
                UserDialogs.Instance.HideLoading();
                //  return result.Groups;
            }

        }
        #endregion
    }
}
