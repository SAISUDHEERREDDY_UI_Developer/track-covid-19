﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Covid19.CustomCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimpleCell : ContentView
    {
        public SimpleCell()
        {
            InitializeComponent();
            var tgr = new TapGestureRecognizer { NumberOfTapsRequired = 1 };
            tgr.Tapped += ViewOn_Clicked;
            this.GestureRecognizers.Add(tgr);

            this.MainFrame.BindingContext = this;
            this.MainGrid.BindingContext = this;
            this.lblTitle.BindingContext = this;
            this.lblBody1.BindingContext = this;
            this.XFLabel.BindingContext = this;
        }
        public event EventHandler CustomClicked;

        public virtual void ViewOn_Clicked(object sender, EventArgs e)
        {
            CustomClicked?.Invoke(sender, e);
        }
        /// <summary>
        /// Binding Image Properties
        /// </summary>


        public static readonly BindableProperty ImageUserProperty = BindableProperty.Create(nameof(ImageUser),
                                                                        typeof(ImageSource),
                                                                        typeof(SimpleCell),
                                                                        defaultBindingMode: BindingMode.TwoWay,
                                                                  propertyChanged: (bindable, oldVal, newVal) =>
                                                                  {
                                                                      var matEntry = (SimpleCell)bindable;
                                                                      matEntry.ImageUserIcon.Source = (ImageSource)newVal;
                                                                  });


        /// <summary>
        /// Binding Text Properties
        /// TextLabel
        /// TextHead
        /// TextTitle
        /// TextBody
        /// TextBottom
        /// </summary>
        public static BindableProperty TextTitleProperty = BindableProperty.Create(nameof(TextTitle),
                                                      typeof(string),
                                                      typeof(SimpleCell),
                                                      defaultBindingMode: BindingMode.TwoWay);
        public static BindableProperty TextBodyProperty = BindableProperty.Create(nameof(TextBody),
                                                    typeof(string),
                                                    typeof(SimpleCell),
                                                    defaultBindingMode: BindingMode.TwoWay);


        /// <summary>
        /// Binding Color Properties
        /// ColorLabel
        /// ColorHead
        /// ColorTitle
        /// ColorBody
        /// ColorBottom
        /// </summary>
        public static BindableProperty ColorTitleProperty = BindableProperty.Create(nameof(ColorTitle),
                                                    typeof(Color),
                                                    typeof(SimpleCell),
                                                    defaultValue: Color.White,
                                                    defaultBindingMode: BindingMode.TwoWay);
        public static BindableProperty ColorBodyProperty = BindableProperty.Create(nameof(ColorBody),
                                                       typeof(Color),
                                                       typeof(SimpleCell),
                                                       defaultValue: Color.White,
                                                       defaultBindingMode: BindingMode.TwoWay);
        public static BindableProperty LineColorProperty = BindableProperty.Create(nameof(LineColor),
                                                        typeof(Color),
                                                        typeof(SimpleCell),
                                                        defaultValue: Color.White,
                                                        defaultBindingMode: BindingMode.TwoWay);

        public static BindableProperty BGColorProperty = BindableProperty.Create(nameof(BGColor),
                                                       typeof(Color),
                                                       typeof(SimpleCell),
                                                       defaultValue: Color.White,
                                                       defaultBindingMode: BindingMode.TwoWay);


        public ImageSource ImageUser
        {
            get => (ImageSource)GetValue(ImageUserProperty);
            set => SetValue(ImageUserProperty, value);
        }


        public string TextTitle
        {
            get => (string)GetValue(TextTitleProperty);
            set => SetValue(TextTitleProperty, value);
        }
        public string TextBody
        {
            get => (string)GetValue(TextBodyProperty);
            set => SetValue(TextBodyProperty, value);
        }

        public Color ColorTitle
        {
            get => (Color)GetValue(ColorTitleProperty);
            set => SetValue(ColorTitleProperty, value);
        }

        public Color ColorBody
        {
            get => (Color)GetValue(ColorBodyProperty);
            set => SetValue(ColorBodyProperty, value);
        }

        public Color LineColor
        {
            get => (Color)GetValue(LineColorProperty);
            set => SetValue(LineColorProperty, value);
        }
        public Color BGColor
        {
            get => (Color)GetValue(BGColorProperty);
            set => SetValue(BGColorProperty, value);
        }
    }
}