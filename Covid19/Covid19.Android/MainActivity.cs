﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Acr.UserDialogs;
using Android.Content;
using System.Threading.Tasks;
using Firebase.Iid;
using Android.Util;

namespace Covid19.Droid
{
    [Activity(Label = "Track Covid-19", Icon = "@mipmap/icon", Theme = "@style/MainTheme",
        MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private NetworkReceiver internetReceiver;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            UserDialogs.Init(this);
            internetReceiver = new NetworkReceiver();
            //internetReceiver.OnReceive(this, this.Intent);
             RegisterReceiver(internetReceiver, new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED"));
             RegisterReceiver(internetReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

            
            Task.Run(() =>
            {
                var instanceid = FirebaseInstanceId.Instance;
                instanceid.DeleteInstanceId();
                Log.Debug("TAG", "{0} {1}", instanceid.Token, instanceid.GetToken(this.GetString(Resource.String.gcm_defaultSenderId),
                    Firebase.Messaging.FirebaseMessaging.InstanceIdScope)); 
            });
            //Window.SetStatusBarColor(Android.Graphics.Color.);
            LoadApplication(new App());
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }





        protected override void OnRestart()
        {
            base.OnRestart();
            RegisterReceiver(internetReceiver, new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED"));
            RegisterReceiver(internetReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        }
        protected override void OnPause()
        {
            UnregisterReceiver(internetReceiver);
            // Code omitted for clarity
            base.OnPause();
        }
    }
}